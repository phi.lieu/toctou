= Time Of Check Time Of Use (TOCTOU)

//tag::abstract[]

TOCTOU is a race condition bug. The checking and usage of a program state
happens on a different state.  If program state changes between check and use
it may result into authorisation bypass or other unexpected results.

//end::abstract[]

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`.
. Run security tests: `make securitytest`.

[NOTE]
--
The last test will fail.
--

=== Task 1

Run the project and give it an amount.
Try to trigger the bug in run-time.
Review the program code try to find out
why security tests fails.

[IMPORTANT]
--
Avoid looking at tests or patch file and try to
spot the security bug on your own.
--

=== Task 2

The program is vulnerable to TOCTOU.
Perform both run-time and static analysis to test for this vulnerability.
Find out how to patch this vulnerability.

=== Task 3

Review `test/programSecurityTest.cs` and see how security tests
works. Review your patch from the previous task.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch:

* Read link:cheatsheet.adoc[] and apply to your solution.
* Review the patched program.
* Run all tests.

=== Task 5

Push you code and make sure build is passed
Finally, send a pull request (PR).

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 11.1.6
* CWE 367

//end::references[]

include::CONTRIBUTING.adoc[]

== License

See link:LICENSE[]
